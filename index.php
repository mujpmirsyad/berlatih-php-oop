<?php
    require_once("Animal.php");
    require_once("Frog.php");
    require_once("Ape.php");


    $sheep = new Animal("shaun");

    echo "Nama : $sheep->name <br>";
    echo "Jumlah kaki : $sheep->legs <br>";
    echo "Darah : $sheep->cold_blooded <br><br>";

    $kodok = new Frog("buduk");
    echo "Nama : $kodok->name <br>";
    echo "Jumlah kaki : $kodok->legs <br>";
    echo "Darah : $kodok->cold_blooded <br>";
    $kodok->jump();

    $sungokong = new Ape("kera sakti");
    echo "Nama : $sungokong->name <br>";
    echo "Jumlah kaki : $sungokong->legs <br>";
    echo "Darah : $sungokong->cold_blooded <br>";
    $sungokong->yell(); // "Auooo"
